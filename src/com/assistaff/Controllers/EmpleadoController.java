/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Controllers;

import com.assistaff.Models.Credencial;
import com.assistaff.Models.Empleado;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Writer;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JComboBox;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author yotok
 */
public class EmpleadoController implements CRUD<Empleado>{

    private final ConnectionSQLite conn;

    public EmpleadoController() {
        conn = new ConnectionSQLite();
    }

    @Override
    public int agregar(Empleado t) {
        int r = conn.ejecutarComando(String.format("INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s,%s,%s) VALUES ('%s','%s','%s','%s','%s','%s',%d,'%s',%d)",
                Empleado.class.getSimpleName(),
                "nombre","apellidoPaterno","apellidoMaterno","sexo","fechaIngreso","fechaRetiro","idCredencial","foto","idCalendario",
                t.getNombre(),t.getApellidoPaterno(),t.getApellidoMaterno(),t.getSexo(),t.getFechaIngreso(),t.getFechaRetiro(),t.getCredencial(),t.getFoto(),t.getCalendario()));
        
        if(r == 0){
            throw new RuntimeException("Ocurrió un problema al agregar, Empleado");
        }
        
        return r;
    }

    @Override
    public int editar(Empleado t) {
        int r = conn.ejecutarComando(String.format("UPDATE %s SET %s='%s',%s='%s',%s='%s',%s='%s',%s='%s',%s='%s',%s=%d,%s='%s',%s=%d WHERE %s=%d",
                Empleado.class.getSimpleName(),
                "nombre",t.getNombre(),
                "apellidoPaterno",t.getApellidoPaterno(),
                "apellidoMaterno",t.getApellidoMaterno(),
                "sexo",t.getSexo(),
                "fechaIngreso",t.getFechaIngreso(),
                "fechaRetiro",t.getFechaRetiro(),
                "idCredencial", t.getCredencial(),
                "foto",t.getFoto(),
                "idCalendario",t.getCalendario(),
                "id",t.getId()));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al actualizar, Empleado");
        
        return r;
    }

    @Override
    public int eliminar(Empleado t) {
        int r = conn.ejecutarComando(String.format("DELETE FROM %s WHERE %s=%d",
                Empleado.class.getSimpleName(),
                "id",t.getId()));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al eliminar, Empleado");
        
        return r;
    }

    @Override
    public Object[] obtener(int id) {
        
        Object[] data = null;
        try {
            ResultSet r = conn.ejecutarSentencia(String.format("SELECT * FROM %s WHERE %s=%d",
                Empleado.class.getSimpleName(),"id",id));
            while(r.next()){
                data = new Object[]{
                    r.getString(1),
                    r.getString(2),
                    r.getString(3),
                    r.getString(4),
                    r.getString(5),
                    r.getString(6),
                    r.getString(7),
                    r.getString(8),
                    r.getString(9),
                    r.getString(10),
                };

            }
        } catch (SQLException ex) {
            Logger.getLogger(EmpleadoController.class.getName()).log(Level.SEVERE, null, ex);
        }
                        
        return data;
    }

    @Override
    public DefaultTableModel obtenerTodos() {
        ResultSet r = conn.ejecutarSentencia(String.format("SELECT * FROM %s", Empleado.class.getSimpleName()));
        
        DefaultTableModel model = new DefaultTableModel(new Object[][]{},
                new String[]{"ID","Apellido Paterno", "Apellido Materno", "Nombre", "Fecha de ingreso","Fecha de retiro"})
        {
            @Override
            public boolean isCellEditable(int row, int column) {
            return false;
        }};
        
        try {
            Object[] row = null;
            while(r.next()){
                row = new Object[]{
                    r.getString(1),
                    r.getString(3),
                    r.getString(4),
                    r.getString(2),
                    r.getString(6),
                    r.getString(7)
                };
                model.addRow(row);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmpleadoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return model;
    }

    public BufferedImage createQR(String data, int size)
    {
        BitMatrix matrix;
        Writer writer = new MultiFormatWriter();
        try {            
            EnumMap<EncodeHintType,String> hints = new EnumMap<>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");            
            matrix = writer.encode(data, BarcodeFormat.QR_CODE, size, size, hints);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(matrix, "jpg", output);
            byte[] data_array = output.toByteArray();
            ByteArrayInputStream input = new ByteArrayInputStream(data_array);
            return ImageIO.read(input);            
        } catch (com.google.zxing.WriterException | IOException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }
    
    public String generarCodigoAsistencia(){
        Date fecha = new Date();
        DateFormat formato = new SimpleDateFormat("HHmmssddyy");
        
        return formato.format(fecha);
    }
    
    public void getCombo(JComboBox<Empleado> comboEmpleado){
        ResultSet r = conn.ejecutarSentencia(String.format("SELECT * FROM %s", Empleado.class.getSimpleName()));
                
        try {
            while(r.next()){
                comboEmpleado.addItem(new Empleado(
                        Integer.parseInt(r.getString(1)), 
                        r.getString(2),
                        r.getString(3),
                        r.getString(4),
                        r.getString(5),
                        r.getString(6),
                        r.getString(7),
                        Integer.parseInt(r.getString(8)),
                        r.getString(9),
                        Integer.parseInt(r.getString(10))));
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmpleadoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public int getIdByCodigoAsistencia(String codigoAsistencia) {
        
        int id = 0;
        try {
            ResultSet r = conn.ejecutarSentencia(String.format("SELECT e.%s FROM %s e JOIN %s c ON c.%s = e.%s WHERE c.%s = '%s'",
                "id",Empleado.class.getSimpleName(),Credencial.class.getSimpleName(),"id",
                "idCredencial","codigoAsistencia", codigoAsistencia));
            while(r.next()){
                id = Integer.parseInt(r.getString(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmpleadoController.class.getName()).log(Level.SEVERE, null, ex);
        }
                        
        return id;
    }
}
