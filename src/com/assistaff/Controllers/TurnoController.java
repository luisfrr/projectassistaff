/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Controllers;

import com.assistaff.Models.Turno;;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author yotok
 */
public class TurnoController implements CRUD<Turno>{

    private final ConnectionSQLite conn;

    public TurnoController() {
        this.conn = new ConnectionSQLite();
    }

    @Override
    public int agregar(Turno t) {
        int r = conn.ejecutarComando(String.format("INSERT INTO %s(%s, %s, %s) VALUES ('%s', '%s', '%s')",
                Turno.class.getSimpleName(),"nombre","horaEntrada", "horaSalida",
                t.getNombre(), t.getHoraEntrada(), t.getHoraSalida()));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al agregar, Turno");
        
        return r;
    }

    @Override
    public int editar(Turno t) {
        int r = conn.ejecutarComando(String.format("UPDATE %s SET %s='%s', %s='%s', %s='%s' WHERE %s=%d",
                Turno.class.getSimpleName(),
                "nombre", t.getNombre(),
                "horaEntrada", t.getHoraEntrada(),
                "horaSalida", t.getHoraSalida(),
                "id",t.getId()));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al actualizar, Turno");
        
        return r;
    }

    @Override
    public int eliminar(Turno t) {
        int r = conn.ejecutarComando(String.format("DELETE FROM %s WHERE %s=%d",
                Turno.class.getSimpleName(),
                "id",t.getId()));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al eliminar, Turno");
        
        return r;
    }

    @Override
    public Object[] obtener(int id) {        
        ResultSet r = conn.ejecutarSentencia(String.format("SELECT * FROM %s WHERE %s=%d",
                Turno.class.getSimpleName(),
                "id", id));
        
        Object[] data = null;
        
        try {
            while(r.next()){
                data = new Object[]{
                    r.getString(1),
                    r.getString(2),
                    r.getString(3),
                    r.getString(4)
                };
            }
        } catch (SQLException ex) {
            Logger.getLogger(TurnoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return data;
    }

    @Override
    public DefaultTableModel obtenerTodos() {
        ResultSet r = conn.ejecutarSentencia(String.format("SELECT * FROM %s", 
                Turno.class.getSimpleName()));
        
        DefaultTableModel model = new DefaultTableModel(new Object[][]{},
                new String[]{"ID","Nombre","Hora de entrada","Hora de salida"})
        {
            @Override
            public boolean isCellEditable(int row, int column) {
            return false;
        }};
        
        try {
            while(r.next()){
                Object[] row = new Object[]{
                    r.getString(1),
                    r.getString(2),
                    r.getString(3),
                    r.getString(4)
                };
                
                model.addRow(row);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TurnoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return model;
    }
}
