/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Models;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author yotok
 */
public class Image {
    
    private static final String CARPETA = "/testnetb/assistaff";
    private static String[] nombreOriginal;
    private static String extension;
    private static int ancho;
    private static int alto;

    public Image() {
    }
    
    public static String GuardarImagen(File file, String nombre){
        CarpetaExistente();
            
        byte[] arrayFoto = new byte[(int) file.length()];
            
        FileInputStream fis;
        try {
            fis = new FileInputStream(file);
            fis.read(arrayFoto);
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Image.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Image.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //nombreOriginal = file.getName().split("\\.");
        extension = "jpg";
            
        String direccion = String.format("%s/%s.%s",CARPETA,nombre,extension);
        Path path = Paths.get(direccion);
        
        try {   			
            Files.write(path,arrayFoto);
        } catch (IOException ex) {
            Logger.getLogger(Image.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return direccion;
    }
    
    public static File redimensionar(File input,String urlImagen) throws IOException {
        
        BufferedImage img = ImageIO.read(input);
        
        java.awt.Image image = new ImageIcon(input.getAbsolutePath()).getImage();
        
        alto = 200;
        ancho = (alto * image.getWidth(null))/image.getHeight(null);
        
        
        java.awt.Image temp = img.getScaledInstance(ancho, alto, java.awt.Image.SCALE_SMOOTH);

        BufferedImage nuevaImagen = new BufferedImage(ancho, alto, BufferedImage.TYPE_INT_ARGB);
	
        Graphics2D g2d = nuevaImagen.createGraphics();
	g2d.drawImage(temp, 0, 0, null);
	g2d.dispose();
	
	File output = new File(urlImagen);
	ImageIO.write(nuevaImagen, extension, output);
        
        return output;
    }
    
    private static void CarpetaExistente(){   
        File directorio = new File(CARPETA);
            
        if(!directorio.exists()) 
	{
	    directorio.mkdirs();
	}
    }
}
