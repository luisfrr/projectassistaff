package com.assistaff.Controllers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sqlite.SQLiteConnection;

/**
 *
 * @author yotok
 */
public class ConnectionSQLite {

    private final String DB_CONNECTION;
    
    private Connection conn;

    public ConnectionSQLite() {
        String db = "./src/com/assistaff/bd.db"; 
        this.DB_CONNECTION = "jdbc:sqlite:" + db;
    }

    public Connection conectar(){
        try {
            Class.forName("org.sqlite.JDBC");
            this.conn = DriverManager.getConnection(DB_CONNECTION);
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Problemas al conectarse " + e);
        }
        return this.conn;
    }
    
    public void desconectar(){
        this.conn = null;
    }
    
    public int ejecutarComando(String sql){
        try {
            PreparedStatement ps = conectar().prepareStatement(sql);
            ps.execute();
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(SQLiteConnection.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    public ResultSet ejecutarSentencia(String sql){
        try {
            PreparedStatement ps = conectar().prepareStatement(sql);
            ps.execute();
            return ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(SQLiteConnection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public boolean isConectado(){
        return conn != null;
    }
    
}