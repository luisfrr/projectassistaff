/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Controllers;

import com.assistaff.Models.TipoEmpleado;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author yotok
 */
public class TipoEmpleadoController  implements CRUD<TipoEmpleado>{

    private ConnectionSQLite conn;

    public TipoEmpleadoController() {
        conn = new ConnectionSQLite();
    }
    
    @Override
    public int agregar(TipoEmpleado t) {
        int r = conn.ejecutarComando(String.format("INSERT INTO %s(%s, %s, %s) VALUES ('%s', '%s', '%s')",
                TipoEmpleado.class.getSimpleName(),
                "codigo","puesto", "descripcion",
                t.getCodigo(), t.getPuesto(), t.getDescripcion()));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al agregar, TipoEmpleado");
        
        return r;
    }

    @Override
    public int editar(TipoEmpleado t) {
        int r = conn.ejecutarComando(String.format("UPDATE %s SET %s='%s', %s='%s', %s='%s' WHERE %s=%d",
                TipoEmpleado.class.getSimpleName(),
                "codigo", t.getCodigo(),
                "puesto", t.getPuesto(),
                "descripcion", t.getDescripcion(),
                "id",t.getId()));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al actualizar, TipoEmpleado");
        
        return r;
    }

    @Override
    public int eliminar(TipoEmpleado t) {
        int r = conn.ejecutarComando(String.format("DELETE FROM %s WHERE %s=%d",
                TipoEmpleado.class.getSimpleName(),
                "id",t.getId()));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al eliminar, TipoEmpleado");
        
        return r;
    }

    @Override
    public Object[] obtener(int id) {
        ResultSet r = conn.ejecutarSentencia(String.format("SELECT * FROM %s WHERE %s=%d",
                TipoEmpleado.class.getSimpleName(),
                "id", id));
        
        Object[] data = null;
        
        try {
            while(r.next()){
                data = new Object[]{
                    r.getString(1),
                    r.getString(2),
                    r.getString(3),
                    r.getString(4)
                };
            }
        } catch (SQLException ex) {
            Logger.getLogger(TipoEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return data;
    }

    @Override
    public DefaultTableModel obtenerTodos() {
        ResultSet r = conn.ejecutarSentencia(String.format("SELECT * FROM %s", 
                TipoEmpleado.class.getSimpleName()));
        
        DefaultTableModel model = new DefaultTableModel(new Object[][]{},
                new String[]{"ID","Código","Puesto","Descripción"})
        {
            @Override
            public boolean isCellEditable(int row, int column) {
            return false;
        }};
        
        try {
            while(r.next()){
                Object[] row = new Object[]{
                    r.getString(1),
                    r.getString(2),
                    r.getString(3),
                    r.getString(4)
                };
                
                model.addRow(row);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TipoEmpleado.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return model;
    }

}
