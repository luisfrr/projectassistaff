/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Controllers;

import com.assistaff.Models.Asistencia;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author yotok
 */
public class AsistenciaController implements CRUD<Asistencia> {

    private final ConnectionSQLite conn;

    public AsistenciaController() {
        conn = new ConnectionSQLite();
    }

    @Override
    public int agregar(Asistencia t) {
        return 0;
    }

    @Override
    public int editar(Asistencia t) {
        return 0;
    }

    @Override
    public int eliminar(Asistencia t) {
        return 0;
    }

    @Override
    public Object[] obtener(int id) {
        return null;
    }

    @Override
    public DefaultTableModel obtenerTodos() {
        return null;
    }
    
    public DefaultTableModel obtenerHistory(String codigo){
        ResultSet r = conn.ejecutarSentencia(String.format("select c.fecha,c.horaLlegada,horaSalida from checador c INNER JOIN empleado e on e.id=c.idEmpleado INNER JOIN Credencial cre on cre.id=e.idCredencial where cre.codigoAsistencia='%s' ORDER BY c.id desc", codigo));
        
        DefaultTableModel model = new DefaultTableModel(new Object[][]{},
                new String[]{"Fecha","Hora de Entrada","Hora de Salida"})
        {
            @Override
            public boolean isCellEditable(int row, int column) {
            return false;
        }};
        
        try {
            while(r.next()){
                Object[] row = new Object[]{
                    r.getString(1),
                    r.getString(2),
                    r.getString(3),
                };
                
                model.addRow(row);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AsistenciaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return model;
    }
    
}
