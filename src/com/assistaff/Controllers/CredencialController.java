/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Controllers;

import com.assistaff.Models.Credencial;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author yotok
 */
public class CredencialController implements CRUD<Credencial>{

    private final ConnectionSQLite conn;

    public CredencialController() {
        conn = new ConnectionSQLite();
    }
    
    @Override
    public int agregar(Credencial t) {
        int r = conn.ejecutarComando(String.format("INSERT INTO %s(%s,%s) VALUES ('%s','%s')",
                "Credencial",
                "codigoAsistencia","codigoQR",
                t.getCodigoAsistencia(),t.getCodigoQR()));
        
        if(r == 0){
            throw new RuntimeException("Ocurrió un problema al agregar, Credencial");
        }
        
        return r;
    }

    @Override
    public int editar(Credencial t) {
        int r = conn.ejecutarComando(String.format("UPDATE %s SET %s='%s', %s='%s' WHERE %s=%d",
                Credencial.class.getSimpleName(),
                "codigoAsistencia",t.getCodigoAsistencia(),
                "codigoQR",t.getCodigoQR(),
                "id",t.getId()));
        
        if(r == 0){
            throw new RuntimeException("Ocurrió un problema al editar, Credencial");
        }
        
        return r;
    }

    @Override
    public int eliminar(Credencial t) {
        int r = conn.ejecutarComando(String.format("DELETE FROM %s WHERE %s=%d",
                Credencial.class.getSimpleName(),
                "id",t.getId()));
        
        if(r == 0){
            throw new RuntimeException("Ocurrió un problema al eliminar, Credencial");
        }
        
        return r;
    }

    @Override
    public Object[] obtener(int id) {
        
        Object[] data = null;
        try {
            ResultSet r = conn.ejecutarSentencia(String.format("SELECT * FROM %s WHERE %s=%d",
                Credencial.class.getSimpleName(),"id",id));
            while(r.next()){
                data = new Object[]{
                    r.getString(1),
                    r.getString(2),
                    r.getString(3)
                };

            }
        } catch (SQLException ex) {
            Logger.getLogger(CredencialController.class.getName()).log(Level.SEVERE, null, ex);
        }
                        
        return data;
    }

    @Override
    public DefaultTableModel obtenerTodos() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getMax(){
        int max = 0;
        try {
            ResultSet r = conn.ejecutarSentencia(String.format("SELECT max(%s) FROM %s",
                "id",Credencial.class.getSimpleName()));
            while(r.next()){
                max = Integer.parseInt(r.getString(1));

            }
        } catch (SQLException ex) {
            Logger.getLogger(CredencialController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return max;
    }
    
    public Object[] obtenerByCodigoAsistencia(String codigo) {
        
        Object[] data = null;
        try {
            ResultSet r = conn.ejecutarSentencia(String.format("SELECT * FROM %s WHERE %s='%s'",
                Credencial.class.getSimpleName(),"codigoAsistencia",codigo));
            while(r.next()){
                data = new Object[]{
                    r.getString(1),
                    r.getString(2),
                    r.getString(3)
                };

            }
        } catch (SQLException ex) {
            Logger.getLogger(CredencialController.class.getName()).log(Level.SEVERE, null, ex);
        }
                        
        return data;
    }
    
    public void sendEmail(String to, String qrPath, String codigoAsistencia){
        
        String from = "luisfrr27@gmail.com";
        final String username = "luisfrr27@gmail.com";
	final String password = "fer221998";

	Properties props = new Properties();
	props.put("mail.smtp.auth", "true");
	props.put("mail.smtp.starttls.enable", "true");
	props.put("mail.smtp.host", "smtp.gmail.com");
	props.put("mail.smtp.port", "587");

	Session session = Session.getInstance(props,
	    new javax.mail.Authenticator() {
                @Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}
	});
        
        try {

            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(from));

            message.setRecipients(Message.RecipientType.TO,
            InternetAddress.parse(to));

            message.setSubject("Assistaff - QR Code");

            
            MimeMultipart multipart = new MimeMultipart("related");

            BodyPart messageBodyPart = new MimeBodyPart();
            String htmlText = "<H1>Hello, Here there are your QR Code</H1><br><br><p>Your assistance code is: "+ codigoAsistencia +"</p><img src=\"cid:image\">";
            messageBodyPart.setContent(htmlText, "text/html");

            multipart.addBodyPart(messageBodyPart);

         
            messageBodyPart = new MimeBodyPart();
            DataSource fds = new FileDataSource(qrPath);

            messageBodyPart.setDataHandler(new DataHandler(fds));
            messageBodyPart.setHeader("Content-ID", "<image>");

            multipart.addBodyPart(messageBodyPart);

            message.setContent(multipart);
            Transport.send(message);

            System.out.println("Sent message successfully....");

      } catch (MessagingException e) {
         throw new RuntimeException(e);
      }
    }
}
