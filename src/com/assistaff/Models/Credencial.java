/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Models;

/**
 *
 * @author yotok
 */
public class Credencial {
    private int id;
    private String codigoAsistencia;
    private String codigoQR;

    public Credencial() {
    }

    public Credencial(int id, String codigoAsistencia, String codigoQR) {
        this.id = id;
        this.codigoAsistencia = codigoAsistencia;
        this.codigoQR = codigoQR;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getCodigoAsistencia() {
        return codigoAsistencia;
    }

    public void setCodigoAsistencia(String codigoAsistencia) {
        this.codigoAsistencia = codigoAsistencia;
    }

    public String getCodigoQR() {
        return codigoQR;
    }

    public void setCodigoQR(String codigoQR) {
        this.codigoQR = codigoQR;
    }
}
