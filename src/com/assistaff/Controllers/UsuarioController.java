/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Controllers;

import com.assistaff.Models.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author yotok
 */
public class UsuarioController implements CRUD<Usuario>{

    private final ConnectionSQLite conn;

    public UsuarioController() {
        conn = new ConnectionSQLite();
    }

    @Override
    public int agregar(Usuario t) {
        int r = conn.ejecutarComando(String.format("INSERT INTO %s(%s, %s, %s, %s) VALUES ('%s', '%s', '%s', %d)",
                Usuario.class.getSimpleName(),
                "nombre","email", "contrasenia","idEmpleado",
                t.getNombre(), t.getEmail(), t.getContrasenia(), t.getIdEmpleado()));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al agregar, Usuario");
        
        return r;
    }

    @Override
    public int editar(Usuario t) {
        int r = conn.ejecutarComando(String.format("UPDATE %s SET %s='%s', %s='%s', %s='%s', %s=%d WHERE %s=%d",
                Usuario.class.getSimpleName(),
                "nombre",t.getNombre(),
                "email", t.getEmail(),
                "contrasenia", t.getContrasenia(),
                "idEmpleado",t.getIdEmpleado(),
                "id", t.getId()));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al editar, Usuario");
        
        return r;
    }

    @Override
    public int eliminar(Usuario t) {
        int r = conn.ejecutarComando(String.format("DELETE FROM %s WHERE %s=%d",
                Usuario.class.getSimpleName(),
                "id", t.getId()));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al eliminar, Usuario");
        
        return r;
    }
    
    public int eliminarByIdEmpleado(int id) {
        int r = conn.ejecutarComando(String.format("DELETE FROM %s WHERE %s=%d",
                Usuario.class.getSimpleName(),
                "idEmpleado", id));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al eliminar, Usuario");
        
        return r;
    }

    @Override
    public Object[] obtener(int id) {
         ResultSet r = conn.ejecutarSentencia(String.format("SELECT * FROM %s WHERE %s=%d",
                Usuario.class.getSimpleName(),
                "id", id));
        
        Object[] data = null;
        
        try {
            while(r.next()){
                data = new Object[]{
                    r.getString(1),
                    r.getString(2),
                    r.getString(3),
                    r.getString(4),
                    r.getString(5)
                };
            }
        } catch (SQLException ex) {
            Logger.getLogger(TurnoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return data;
    }

    @Override
    public DefaultTableModel obtenerTodos() {
        ResultSet r = conn.ejecutarSentencia("SELECT u.id, u.nombre, u.email, ( e.nombre || ' '  ||e.apellidoPaterno) AS empleado FROM Usuario u JOIN Empleado e ON e.id = u.idEmpleado");
        
        DefaultTableModel model = new DefaultTableModel(new Object[][]{},
                new String[]{"ID","Nombre","Email","Empleado"})
        {
            @Override
            public boolean isCellEditable(int row, int column) {
            return false;
        }};
        
        try {
            while(r.next()){
                Object[] row = new Object[]{
                    r.getString(1),
                    r.getString(2),
                    r.getString(3),
                    r.getString(4)
                };
                
                model.addRow(row);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TurnoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return model;
    }
}
