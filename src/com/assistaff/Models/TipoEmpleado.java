/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Models;

/**
 *
 * @author yotok
 */
public class TipoEmpleado {
    private int id;
    private String codigo;
    private String puesto;
    private String descripcion;

    public TipoEmpleado() {
    }

    public TipoEmpleado(int id, String codigo, String puesto, String descripcion) {
        this.id = id;
        this.codigo = codigo;
        this.puesto = puesto;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
