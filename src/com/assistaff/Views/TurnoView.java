/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Views;

import com.assistaff.Controllers.TurnoController;
import com.assistaff.Models.Turno;
import java.text.ParseException;
import javax.swing.JTable;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author yotok
 */
public class TurnoView extends javax.swing.JPanel {

    private final TurnoController controller;
    /**
     * Creates new form TurnoView
     */
    public TurnoView() {
        initComponents();
        controller = new TurnoController();
        ActualizarTabla();
        LimpiarControles();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jbAgregar = new javax.swing.JButton();
        jbActualizar = new javax.swing.JButton();
        jbEliminar = new javax.swing.JButton();
        jtfNombre = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtbTurno = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jbLimpiar = new javax.swing.JButton();
        jlId = new javax.swing.JLabel();
        jtfHoraEntrada = new javax.swing.JFormattedTextField();
        jtfHoraSalida = new javax.swing.JFormattedTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(960, 640));
        setMinimumSize(new java.awt.Dimension(960, 640));
        setPreferredSize(new java.awt.Dimension(960, 640));

        jLabel3.setFont(new java.awt.Font("Montserrat", 0, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(21, 100, 191));
        jLabel3.setText("Turnos");

        jbAgregar.setBackground(new java.awt.Color(0, 118, 108));
        jbAgregar.setFont(new java.awt.Font("Montserrat", 0, 16)); // NOI18N
        jbAgregar.setForeground(new java.awt.Color(255, 255, 255));
        jbAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/assistaff/resources/tick_white.png"))); // NOI18N
        jbAgregar.setText("Agregar");
        jbAgregar.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/com/assistaff/resources/tick_white.png"))); // NOI18N
        jbAgregar.setPreferredSize(new java.awt.Dimension(120, 32));
        jbAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Agregar(evt);
            }
        });

        jbActualizar.setBackground(new java.awt.Color(188, 81, 0));
        jbActualizar.setFont(new java.awt.Font("Montserrat", 0, 16)); // NOI18N
        jbActualizar.setForeground(new java.awt.Color(255, 255, 255));
        jbActualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/assistaff/resources/refresh.png"))); // NOI18N
        jbActualizar.setText("Actualizar");
        jbActualizar.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/com/assistaff/resources/refresh.png"))); // NOI18N
        jbActualizar.setPreferredSize(new java.awt.Dimension(120, 32));
        jbActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Actualizar(evt);
            }
        });

        jbEliminar.setBackground(new java.awt.Color(154, 0, 7));
        jbEliminar.setFont(new java.awt.Font("Montserrat", 0, 16)); // NOI18N
        jbEliminar.setForeground(new java.awt.Color(255, 255, 255));
        jbEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/assistaff/resources/trash.png"))); // NOI18N
        jbEliminar.setText("Eliminar");
        jbEliminar.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/com/assistaff/resources/trash.png"))); // NOI18N
        jbEliminar.setPreferredSize(new java.awt.Dimension(120, 32));
        jbEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Eliminar(evt);
            }
        });

        jtfNombre.setFont(new java.awt.Font("Montserrat", 0, 16)); // NOI18N
        jtfNombre.setForeground(new java.awt.Color(102, 100, 98));

        jtbTurno.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jtbTurno.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                VerDato(evt);
            }
        });
        jScrollPane1.setViewportView(jtbTurno);

        jLabel5.setFont(new java.awt.Font("Montserrat", 0, 16)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(102, 100, 98));
        jLabel5.setText("Hora de entrada:");

        jLabel6.setFont(new java.awt.Font("Montserrat", 0, 16)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(102, 100, 98));
        jLabel6.setText("Nombre del turno:");

        jLabel7.setFont(new java.awt.Font("Montserrat", 0, 16)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(102, 100, 98));
        jLabel7.setText("Hora de salida:");

        jbLimpiar.setBackground(new java.awt.Color(21, 100, 191));
        jbLimpiar.setFont(new java.awt.Font("Montserrat", 0, 16)); // NOI18N
        jbLimpiar.setForeground(new java.awt.Color(255, 255, 255));
        jbLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/assistaff/resources/eraser.png"))); // NOI18N
        jbLimpiar.setText("Limpiar");
        jbLimpiar.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/com/assistaff/resources/tick_white.png"))); // NOI18N
        jbLimpiar.setPreferredSize(new java.awt.Dimension(120, 32));
        jbLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Limpiar(evt);
            }
        });

        jlId.setFont(new java.awt.Font("Montserrat", 0, 16)); // NOI18N
        jlId.setForeground(new java.awt.Color(102, 100, 98));
        jlId.setText("id");
        jlId.setVisible(false);

        jtfHoraEntrada.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jtfHoraEntrada.setToolTipText("Formato: ##:## Ejemplo: 07:00");
        jtfHoraEntrada.setFont(new java.awt.Font("Roboto Thin", 0, 16)); // NOI18N

        jtfHoraSalida.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jtfHoraSalida.setToolTipText("Formato: ##:## Ejemplo: 13:00");
        jtfHoraSalida.setFont(new java.awt.Font("Roboto Thin", 0, 16)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jbAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jbLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jbActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jbEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addGap(203, 203, 203))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jtfHoraEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jtfHoraSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap(226, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 656, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jtfNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jlId, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(72, 72, 72)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlId))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfHoraEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfHoraSalida, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(44, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    public void ActualizarTabla(){
        jtbTurno.setModel(controller.obtenerTodos());
    }
    
    private Turno getDatos(){
        Turno t = new Turno();
        
        if(jtfNombre.getText().equals("")||jtfHoraEntrada.getText().equals("  :  ")||
                jtfHoraSalida.getText().equals("  :  ")){
            throw new RuntimeException("No se permiten campos vacios");
        }else{
            
            t.setId(jlId.getText().equals("")?0:Integer.parseInt(jlId.getText()));
            t.setNombre(jtfNombre.getText());
            t.setHoraEntrada(jtfHoraEntrada.getText());
            t.setHoraSalida(jtfHoraSalida.getText());
            
            return t;
        }

    }
    
    public void LimpiarControles(){
        jlId.setText("");
        jtfNombre.setText("");
        jtfHoraEntrada.setText("");
        jtfHoraSalida.setText("");
        
        MaskFormatter formatter;
        try{
            formatter = new MaskFormatter("##:##");
            formatter.install(jtfHoraEntrada);
            
        }catch(ParseException e){

        }
        
        try{
            formatter = new MaskFormatter("##:##");
            formatter.install(jtfHoraSalida);
            
        }catch(ParseException e){

        }

        jbAgregar.setEnabled(true);
        jbActualizar.setEnabled(false);
        jbEliminar.setEnabled(false);
    }
    
    private void Agregar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Agregar
        try {
            int r = controller.agregar(getDatos());
            
            showDialog("Operación exitosa", "Se ha agregado correctamente la información.");
            LimpiarControles();
            ActualizarTabla();
        } catch (RuntimeException e) {
            showDialog("Lo sentimos :c", e.getMessage());
        }
    }//GEN-LAST:event_Agregar

    private void Limpiar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Limpiar
        LimpiarControles();
    }//GEN-LAST:event_Limpiar

    private void Actualizar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Actualizar
        try {
            int r = controller.editar(getDatos());
            
            showDialog("Operación exitosa", "Se ha actualizado correctamente la información.");
            LimpiarControles();
            ActualizarTabla();
        } catch (RuntimeException e) {
            showDialog("Lo sentimos :c", e.getMessage());
        }
    }//GEN-LAST:event_Actualizar

    private void Eliminar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Eliminar
        try {
            int r = controller.eliminar(getDatos());
            
            showDialog("Operación exitosa", "Se ha eliminado correctamente la información.");
            LimpiarControles();
            ActualizarTabla();
        } catch (RuntimeException e) {
            showDialog("Lo sentimos :c", e.getMessage());
        }
    }//GEN-LAST:event_Eliminar

    private void VerDato(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_VerDato
        // TODO add your handling code here:
        int fila = jtbTurno.getSelectedRow();
        
        JTable target = (JTable)evt.getSource();
        jlId.setText(target.getValueAt(fila, 0).toString());
        
        Object[] data;
            
        data = controller.obtener(Integer.valueOf(jlId.getText()));
        
        if(data != null){
            jlId.setText(data[0].toString());
            jtfNombre.setText(data[1].toString());
            jtfHoraEntrada.setText(data[2].toString());
            jtfHoraSalida.setText(data[3].toString());
        }
        
        jbAgregar.setEnabled(false);
        jbActualizar.setEnabled(true);
        jbEliminar.setEnabled(true);
    }//GEN-LAST:event_VerDato

    private void showDialog(String title, String message){
        DialogOneButtonView dialog = DialogOneButtonView.getInstance();
        dialog.setTitleAndMessage(title, message);
        dialog.setVisible(true);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton jbActualizar;
    private javax.swing.JButton jbAgregar;
    private javax.swing.JButton jbEliminar;
    private javax.swing.JButton jbLimpiar;
    private javax.swing.JLabel jlId;
    private javax.swing.JTable jtbTurno;
    private javax.swing.JFormattedTextField jtfHoraEntrada;
    private javax.swing.JFormattedTextField jtfHoraSalida;
    private javax.swing.JTextField jtfNombre;
    // End of variables declaration//GEN-END:variables
}
