/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Views;

import com.assistaff.Controllers.CredencialController;
import com.assistaff.Models.Credencial;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;



public class CredencialView extends javax.swing.JFrame {

    private static CredencialView instance;
    private Credencial credencial;
    private final CredencialController credencialController;
    
    private CredencialView() {
        credencialController = new CredencialController();
        super.setUndecorated(true);
        initComponents();
        super.setBackground(Color.WHITE);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PanelTitulo = new javax.swing.JPanel();
        LabelTitulo = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        PanelCredencialFront = new javax.swing.JPanel();
        LabelLogo = new javax.swing.JLabel();
        jlFoto = new javax.swing.JLabel();
        jlNombre = new javax.swing.JLabel();
        jlOtraCosa = new javax.swing.JLabel();
        PanelCredencialBack = new javax.swing.JPanel();
        jlQR = new javax.swing.JLabel();
        jlCodigoAsistencia = new javax.swing.JLabel();
        PanelImprimir = new javax.swing.JPanel();
        LabelImprimir = new javax.swing.JLabel();
        jtfCorreo = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Credencial");
        setBackground(new java.awt.Color(255, 255, 255));
        setType(java.awt.Window.Type.POPUP);

        PanelTitulo.setBackground(new java.awt.Color(21, 100, 191));
        PanelTitulo.setPreferredSize(new java.awt.Dimension(194, 40));

        LabelTitulo.setFont(new java.awt.Font("Montserrat", 0, 18)); // NOI18N
        LabelTitulo.setForeground(new java.awt.Color(255, 255, 255));
        LabelTitulo.setText("Credencial");
        LabelTitulo.setToolTipText("");
        LabelTitulo.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jPanel1.setBackground(new java.awt.Color(226, 17, 34));
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Close(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/assistaff/resources/cancel.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel1)
                .addGap(16, 16, 16))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel1)
                .addGap(8, 8, 8))
        );

        javax.swing.GroupLayout PanelTituloLayout = new javax.swing.GroupLayout(PanelTitulo);
        PanelTitulo.setLayout(PanelTituloLayout);
        PanelTituloLayout.setHorizontalGroup(
            PanelTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelTituloLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(LabelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        PanelTituloLayout.setVerticalGroup(
            PanelTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelTituloLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(LabelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(PanelTituloLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        PanelCredencialFront.setBackground(new java.awt.Color(43, 38, 40));

        LabelLogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/assistaff/resources/LogoForHome.png"))); // NOI18N

        jlFoto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/assistaff/resources/User.png"))); // NOI18N

        jlNombre.setFont(new java.awt.Font("Montserrat", 0, 24)); // NOI18N
        jlNombre.setForeground(new java.awt.Color(255, 255, 255));
        jlNombre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlNombre.setText("Nombre del Empleado");

        jlOtraCosa.setFont(new java.awt.Font("Montserrat", 0, 16)); // NOI18N
        jlOtraCosa.setForeground(new java.awt.Color(255, 255, 255));
        jlOtraCosa.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlOtraCosa.setText("Turno");

        javax.swing.GroupLayout PanelCredencialFrontLayout = new javax.swing.GroupLayout(PanelCredencialFront);
        PanelCredencialFront.setLayout(PanelCredencialFrontLayout);
        PanelCredencialFrontLayout.setHorizontalGroup(
            PanelCredencialFrontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelCredencialFrontLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelCredencialFrontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlFoto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LabelLogo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addComponent(jlNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(PanelCredencialFrontLayout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addComponent(jlOtraCosa, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PanelCredencialFrontLayout.setVerticalGroup(
            PanelCredencialFrontLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelCredencialFrontLayout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addComponent(LabelLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(jlFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jlNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jlOtraCosa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(32, 32, 32))
        );

        PanelCredencialBack.setBackground(new java.awt.Color(43, 38, 40));

        jlQR.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlQR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/assistaff/resources/Codigo QR.png"))); // NOI18N

        jlCodigoAsistencia.setFont(new java.awt.Font("Montserrat", 0, 10)); // NOI18N
        jlCodigoAsistencia.setForeground(new java.awt.Color(255, 255, 255));
        jlCodigoAsistencia.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlCodigoAsistencia.setText("Codigo asisitencia");

        javax.swing.GroupLayout PanelCredencialBackLayout = new javax.swing.GroupLayout(PanelCredencialBack);
        PanelCredencialBack.setLayout(PanelCredencialBackLayout);
        PanelCredencialBackLayout.setHorizontalGroup(
            PanelCredencialBackLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jlQR, javax.swing.GroupLayout.DEFAULT_SIZE, 362, Short.MAX_VALUE)
            .addComponent(jlCodigoAsistencia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        PanelCredencialBackLayout.setVerticalGroup(
            PanelCredencialBackLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelCredencialBackLayout.createSequentialGroup()
                .addGap(234, 234, 234)
                .addComponent(jlQR, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlCodigoAsistencia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(46, 46, 46))
        );

        PanelImprimir.setBackground(new java.awt.Color(21, 100, 191));
        PanelImprimir.setPreferredSize(new java.awt.Dimension(200, 32));

        LabelImprimir.setFont(new java.awt.Font("Montserrat", 0, 18)); // NOI18N
        LabelImprimir.setForeground(new java.awt.Color(255, 255, 255));
        LabelImprimir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelImprimir.setText("Enviar email");
        LabelImprimir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                SendEmail(evt);
            }
        });

        javax.swing.GroupLayout PanelImprimirLayout = new javax.swing.GroupLayout(PanelImprimir);
        PanelImprimir.setLayout(PanelImprimirLayout);
        PanelImprimirLayout.setHorizontalGroup(
            PanelImprimirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LabelImprimir, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
        );
        PanelImprimirLayout.setVerticalGroup(
            PanelImprimirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LabelImprimir, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
        );

        jtfCorreo.setFont(new java.awt.Font("Montserrat", 0, 16)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PanelTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, 740, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(PanelCredencialFront, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(PanelCredencialBack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jtfCorreo)
                        .addGap(18, 18, 18)
                        .addComponent(PanelImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(24, 24, 24))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(PanelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PanelCredencialFront, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PanelCredencialBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(PanelImprimir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jtfCorreo))
                .addGap(35, 35, 35))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void Close(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_Close
        this.dispose();
    }//GEN-LAST:event_Close

    private void SendEmail(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SendEmail
        try{
            credencialController.sendEmail(jtfCorreo.getText().trim(),
                    credencial.getCodigoQR(), credencial.getCodigoAsistencia());
            
            showDialog("Email enviado", "El Código QR se ha enviado satisfactoriamente");
        }catch(RuntimeException e){
            showDialog("Ocurrió un problema :c", "Verifique su conexión a Internet");
        }
        
    }//GEN-LAST:event_SendEmail

    public void setData(Credencial credencial, String foto, String nombre, String otraCosa){
        this.credencial = credencial;
        File qr = new File(credencial.getCodigoQR());
        try {
            jlQR.setIcon(new ImageIcon(ImageIO.read(qr)));
        } catch (IOException ex) {
            Logger.getLogger(EmpleadoView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        File image = new File(foto);
        try {
            jlFoto.setIcon(new ImageIcon(ImageIO.read(image)));
        } catch (IOException ex) {
            Logger.getLogger(EmpleadoView.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        jlNombre.setText(nombre);
        jlOtraCosa.setText(otraCosa);
        jlCodigoAsistencia.setText(credencial.getCodigoAsistencia());
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CredencialView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CredencialView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CredencialView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CredencialView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new CredencialView().setVisible(true);
        });
    }
    
    private void showDialog(String title, String message){
        DialogOneButtonView dialog = DialogOneButtonView.getInstance();
        dialog.setTitleAndMessage(title, message);
        dialog.setVisible(true);
    }
    
    public static CredencialView getInstance(){
        if (instance == null)
            instance = new CredencialView();
        
        return instance;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelImprimir;
    private javax.swing.JLabel LabelLogo;
    private javax.swing.JLabel LabelTitulo;
    private javax.swing.JPanel PanelCredencialBack;
    private javax.swing.JPanel PanelCredencialFront;
    private javax.swing.JPanel PanelImprimir;
    private javax.swing.JPanel PanelTitulo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel jlCodigoAsistencia;
    private javax.swing.JLabel jlFoto;
    private javax.swing.JLabel jlNombre;
    private javax.swing.JLabel jlOtraCosa;
    private javax.swing.JLabel jlQR;
    private javax.swing.JTextField jtfCorreo;
    // End of variables declaration//GEN-END:variables
}
