/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Models;

/**
 *
 * @author yotok
 */
public class Asistencia {
    private long id;
    private long totalDias;
    private long totalFaltas;

    public Asistencia() {
    }

    public Asistencia(long id, long totalDias, long totalFaltas) {
        this.id = id;
        this.totalDias = totalDias;
        this.totalFaltas = totalFaltas;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTotalDias() {
        return totalDias;
    }

    public void setTotalDias(long totalDias) {
        this.totalDias = totalDias;
    }

    public long getTotalFaltas() {
        return totalFaltas;
    }

    public void setTotalFaltas(long totalFaltas) {
        this.totalFaltas = totalFaltas;
    }
}
