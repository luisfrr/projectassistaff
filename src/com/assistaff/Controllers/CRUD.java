/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Controllers;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author yotok
 * @param <T>
 */
public interface CRUD<T> {
    int agregar(T t);
    int editar(T t);
    int eliminar(T t);
    Object[] obtener(int id);
    DefaultTableModel obtenerTodos();
}
