/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assistaff.Controllers;

import com.assistaff.Models.Checador;
import com.assistaff.Models.Credencial;
import com.assistaff.Models.Empleado;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author yotok
 */
public class ChecadorController implements CRUD<Checador> {

    private final ConnectionSQLite conn;

    public ChecadorController() {
        conn = new ConnectionSQLite();
    }

    @Override
    public int agregar(Checador t) {
        int r = conn.ejecutarComando(String.format("INSERT INTO %s (%s,%s,%s,%s) VALUES ('%s','%s','%s',%d)",
                Checador.class.getSimpleName(),
                "fecha","horaLlegada","horaSalida","idEmpleado",
                t.getFecha(),t.getHoraLlegada(),t.getHoraSalida(),t.getIdEmpleado()));
        
        if(r == 0){
            throw new RuntimeException("Ocurrió un problema al agregar, Checador");
        }
        
        return r;
    }

    @Override
    public int editar(Checador t) {
        int r = conn.ejecutarComando(String.format("UPDATE %s SET %s='%s',%s='%s',%s='%s',%s=%d WHERE %s=%d",
                Checador.class.getSimpleName(),
                "fecha",t.getFecha(),
                "horaLlegada",t.getHoraLlegada(),
                "horaSalida",t.getHoraSalida(),
                "idEmpleado",t.getIdEmpleado(),
                "id",t.getId()));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al actualizar, Checador");
        
        return r;
    }

    @Override
    public int eliminar(Checador t) {
        int r = conn.ejecutarComando(String.format("DELETE FROM %s WHERE %s=%d",
                Checador.class.getSimpleName(),
                "id",t.getId()));
        
        if(r == 0)
            throw new RuntimeException("Ocurrió un problema al eliminar, Checador");
        
        return r;
    }

    @Override
    public Object[] obtener(int id) {
        Object[] data = null;
        try {
            ResultSet r = conn.ejecutarSentencia(String.format("SELECT * FROM %s WHERE %s=%d",
                Checador.class.getSimpleName(),"id",id));
            while(r.next()){
                data = new Object[]{
                    r.getString(1),
                    r.getString(2),
                    r.getString(3),
                    r.getString(4),
                    r.getString(5)
                };

            }
        } catch (SQLException ex) {
            Logger.getLogger(EmpleadoController.class.getName()).log(Level.SEVERE, null, ex);
        }
                        
        return data;
    }

    @Override
    public DefaultTableModel obtenerTodos() {
        ResultSet r = conn.ejecutarSentencia(String.format("SELECT c.id ,c.fecha, c.horaLlegada, c.horaSalida, (e.apellidoPaterno || ' ' || e.apellidoMaterno || ' ' || e.nombre) AS empleado FROM %s c JOIN Empleado e ON e.id = c.idEmpleado", Checador.class.getSimpleName()));
        
        DefaultTableModel model = new DefaultTableModel(new Object[][]{},
                new String[]{"ID","Fecha", "Hora de Llegada", "Hora de Salida", "Empleado"})
        {
            @Override
            public boolean isCellEditable(int row, int column) {
            return false;
        }};
        
        try {
            Object[] row = null;
            while(r.next()){
                row = new Object[]{
                    r.getString(1),
                    r.getString(2),
                    r.getString(3),
                    r.getString(4),
                    r.getString(5)
                };
                model.addRow(row);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmpleadoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return model;
    }
    
    public Object[] getByCodigoAsistenciaAndDate(String codigo, String fecha){
        
        Object[] data = null;
        try {
            ResultSet r = conn.ejecutarSentencia(String.format("SELECT c.* FROM %s c JOIN %s e ON e.%s = c.%s JOIN %s t ON t.%s = e.%s WHERE t.%s = '%s' AND c.%s = '%s'",
                Checador.class.getSimpleName(),
                Empleado.class.getSimpleName(),
                "id","idEmpleado",
                Credencial.class.getSimpleName(),
                "id","idCredencial",
                "codigoAsistencia", codigo,
                "fecha",fecha
                ));
            
            while(r.next()){
                data = new Object[]{
                    r.getString(1),
                    r.getString(2),
                    r.getString(3),
                    r.getString(4),
                    r.getString(5)
                };

            }
        } catch (SQLException ex) {
            Logger.getLogger(EmpleadoController.class.getName()).log(Level.SEVERE, null, ex);
        }
                        
        return data;        
                
    }
           
    
}
